var File = Java.type("java.io.File")
var System = Java.type("java.lang.System")

platf.addSoftwareLibrary(new File("./sqlite-jdbc-3.20.0.jar"))

var tokenizer = require("./tokenizer")
var trfuncs = require("lib/etl/transformations")

var rules = {
    movdiario: {
        calendarioregular: trfuncs.br_date_to_number,
        ean: trfuncs.string_to_integer,
        quantidade: trfuncs.br_to_us_decimal_point,
        valorvendido: trfuncs.br_to_us_decimal_point
    },
    targetcob: {
        codigoae: trfuncs.string_to_integer,
        quatidadecob: trfuncs.string_to_integer,
        mes: trfuncs.year_month_to_number
    }
}

var dml = {
    create: {
        table: {
            movdiario: "CREATE TABLE IF NOT EXISTS movdiario (id INTEGER PRIMARY KEY, calendarioregular INTEGER, ae TEXT NOT NULL, cnpj TEXT NOT NULL, razaosocial TEXT NOT NULL, uf TEXT NOT NULL, cidade TEXT NOT NULL, endereco TEXT NOT NULL, segmentacao TEXT NOT NULL, commodity TEXT NOT NULL, ean INTEGER NOT NULL, marca TEXT NOT NULL, quantidade NUMERIC NOT NULL, valorvendido NUMERIC NOT NULL, tamojunto  TEXT NOT NULL)",
            targetcob: "CREATE TABLE IF NOT EXISTS targetcob( id INTEGER PRIMARY KEY, codigoae INTEGER, descricaoae TEXT, quatidadecob INTEGER, mes INTEGER )"
        }
    }
}

var cmd_bulk_insert = {
    clean: function() { 
        this.cmd_insert = this.cmd_bkp
        this.values = [] 
    },
    cmd_insert: "",
    cmd_bkp: "",
    values: [],
    get_cmd_insert: function () {
        this.cmd_bkp = this.cmd_insert

        return [this.cmd_insert].concat(this.values.join(", ")).join("")
    }
}


function mount_tuple_to_insert(table, values, fields) {
    var rec = [].concat(
        values.map(
            function (value, idx) {
                var transformation = rules[table][fields[idx]]
                return (transformation)? transformation(value) :  (["'", value, "'"]).join("")
            })
    )
    return ["(", rec.join(","), ")"].join("")    
}


function insert_from_line(options, line) {
    var sqlite = options.sqlite
    var values = line.split(options.colsep)
    var tuple = mount_tuple_to_insert("movdiario", values, options.fields)
    var rowid = options.rowid.next().value

    cmd_bulk_insert.values.push(tuple)
    
    if ((rowid % 10000) == 0) {
        try {
            var sql_cmd = cmd_bulk_insert.get_cmd_insert()  

            System.out.print("\r" + rowid)
            sqlite.execute(sql_cmd)
            print(sql_cmd)
            cmd_bulk_insert.clean()

        } catch (e) {
            print("\u001B[31m" + "Erro insert_lines => " + "\u001B[0m", e)
            print(sql_cmd)
            throw e
        }
    }
}


function insert_from_array(options, values) {
    var sqlite = options.sqlite
    var tuple = mount_tuple_to_insert("targetcob", values, options.fields)
    var rowid = options.rowid.next().value

    cmd_bulk_insert.values.push(tuple)
    
    if ((rowid % 10000) == 0) {
        try {
            var sql_cmd = cmd_bulk_insert.get_cmd_insert()  

            System.out.print("\r" + rowid + " ")
            sqlite.execute(sql_cmd)
            // print(sql_cmd)
            cmd_bulk_insert.clean()

        } catch (e) {
            print("\u001B[31m" + "Erro insert_lines => " + "\u001B[0m", e)
            print(sql_cmd)
            throw e
        }
    }
}

function run_etl_txt(db, params) {
   
    tokenizer({colsep: "|", rowsep: /[\r\n]+/g /*, fields: fields*/, sqlite: db})
        .set_input_text_file('./arquivos/2017080701344784151998.txt')
        // .set_input_stream(fs.lines('./test.txt'))
        .take_fields_from_header_row()
        .before_for_each(function beginTransaction(options) {
            cmd_bulk_insert.cmd_insert = "INSERT INTO movdiario (calendarioregular,ae,cnpj,razaosocial,uf,cidade,endereco,segmentacao,commodity,ean,marca,quantidade,valorvendido,tamojunto) VALUES "
        })
        .for_each(insert_from_line)
        .after_for_each(function endTransaction(options) {
            db.execute( cmd_bulk_insert.get_cmd_insert() )
        })
}


function run_etl_xlsx(db, params) {

    tokenizer({sqlite: db})
        // .set_input_xlsx_file('./arquivos/target_cob_grid_08_08_17.xlsx', 1)
        .set_input_xlsx_file('./arquivos/Vinculos_Julho17_070817.xlsx')
        .take_fields_from_header_row()
        .before_for_each(function beginTransaction(options) {
            // cmd_bulk_insert.cmd_insert = "INSERT INTO targetcob (codigoae, descricaoae, quatidadecob, mes) VALUES "
            cmd_bulk_insert.cmd_insert = "INSERT INTO vinculos (codigoae, descricaoae, quatidadecob, mes) VALUES "
        })
        .for_each(insert_from_array)
        .after_for_each(function endTransaction(options) {
            db.execute( cmd_bulk_insert.get_cmd_insert() )
        })
}


function test() {

    var di = new Date().getTime()
    var df

    db.execute(dml.create.table.movdiario)
    db.execute(dml.create.table.targetcob)    
    db.executeInSingleTransaction(run_etl_xlsx)
    df = new Date().getTime()
 
    print("\nTempo de execução => ", (df - di)/1000, "s")
}


exports = {
    test: test
}
