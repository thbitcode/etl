var transform_functions = {

    br_date_to_number: function (value) {
        var fator = 1000000
        
        return value.split("/")
            .reverse()
            .map(function(d){ fator /= 100; return parseInt(d)*fator})
            .reduce(function(prev, curr){ return prev + curr  }, 0)
    },

    year_month_to_number: function (value) {
        var fator = 10000
        
        return value.split("-")
            .map(function(d){ fator /= 100; return parseInt(d)*fator})
            .reduce(function(prev, curr){ return prev + curr  }, 0)
    },

    br_to_us_decimal_point: function (value) {
        return parseFloat(value.replace(",","."))
    },

    string_to_integer: function (value) {
        var i = parseInt(value)
        return (isNaN(i))? 0 : i
    }
    
}

exports = transform_functions
